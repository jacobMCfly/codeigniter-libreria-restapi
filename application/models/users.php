<?php

class Users extends CI_model
{
    public function __construct(){
	 
		parent::__construct();
		  $this->load->database(); 

    }
    
    public function findAll(){
		$this->db->select();
		$this->db->from('users');
		$query = $this->db->get();
		return $query->result();
	}
}
