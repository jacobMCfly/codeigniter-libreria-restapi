<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Restserver\Libraries\REST_Controller;
require APPPATH . '/libraries/REST_Controller.php';
require APPPATH . '/libraries/Format.php';

class Restserver extends REST_Controller {

	public function __construct(){
	 
		parent::__construct();
		  $this->load->database();
		  $this->load->model('Users');

	}

	public function test_get(){
		$this->response($this->Users->findAll());
	}

	// public function test_get(){
	// 	$array = array("hola","mundo","codeigniter");
	// 	$this->response($array);
	// }
}
